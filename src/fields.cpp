#include <sstream>
#include "fields.h"

//debug
#include <iostream>

namespace Rci
{

DeviceMK1::Fields::Fields() :
  _Xposition(0), _Xlength(0), _Yposition(0), _changed(false), _scroll(false), _scrolled(false),
  _scrollPosition(0)
{;}


/** defines a field with the given position and lengths
 *  @param[in] inXposition : position on the screen
 *  @param[in] inXlength : length of the field
 *  @param[in] inYposition : line of the screen
 */
DeviceMK1::Fields::Fields( int inXposition, int inXlength, int inYposition ) :
  _Xposition(inXposition), _Xlength(inXlength), _Yposition(inYposition), _changed(false), _scroll(false),
  _scrolled(false), _scrollPosition(0)
{;}

/** copy constructor
 */
DeviceMK1::Fields::Fields( const Fields &inField ) :
  _value(inField._value), _printableValue(inField._printableValue), _Xposition(inField._Xposition),
  _Xlength(inField._Xlength), _Yposition(inField._Yposition), _changed(inField._changed),
  _scroll(inField._scroll), _scrolled(inField._scrolled), _scrollPosition(inField._scrollPosition),
  _fieldElements(inField._fieldElements)
{;}

/** sets the string to display
 *  @param[in] inString : value to set
 */
void DeviceMK1::Fields::setValue( const std::string &inString, int inVariableNumber )
{
  for( int i = 0; i < _fieldElements.size(); ++i )
  {
    if(_fieldElements[i]._variableIndex == inVariableNumber)
    {
      if( _fieldElements[i]._variableValue != inString && _fieldElements[i]._useVariable)
      {
        _fieldElements[i]._variableValue = inString;
        _changed = true;
      }
    }
  }
}

/** scroll the data to print
 */
int DeviceMK1::Fields::scroll()
{
  if( _scroll && !_changed)
  { 
    ++_scrollPosition;
    if(_scrollPosition >= _value.size() + 2)
      _scrollPosition = 0;

    // gets the scrolled data from the scroll position to the end of _parameterValue
    if( _scrollPosition <= _value.size() )
    {
      _printableValue = _value.substr(_scrollPosition, _Xlength);
      
      int missingChars = _Xlength - _printableValue.size();
      if( missingChars == 2 )
        _printableValue += "  ";
      else if( missingChars == 1 )
        _printableValue += " ";
      else if( missingChars > 2 )
      {
        // the displayed data must be completed by the begining of _parameterValue
        _printableValue += "  ";
        _printableValue += _value.substr(0, missingChars - 2);
      }
    }
    else
    {
      std::string tmpString = "  ";
      tmpString += _value;
      _printableValue = tmpString.substr(_scrollPosition - _value.size(), _Xlength);
    }
    _scrolled = true;
  }
}

/** Prepare the elements vector which will be used to make the printable data
 *  @param[in] rawString : string used to build the elements vector
 */
void DeviceMK1::Fields::makeElements( const std::string &rawString )
{
  int j = 0;
  int lastPosition = 0;
  while( j < rawString.size() && lastPosition < rawString.size() )
  {
    FieldStringElements tmpValue;
    tmpValue._useVariable = false;
    
    j = rawString.find('#', lastPosition);
      
    if( j != std::string::npos && j < rawString.size() )
    {
      int endNum = rawString.find_first_not_of( "0123456789", j+1, 10 );
      if( endNum == std::string::npos )
        endNum = rawString.size();
      
      if( endNum != j+1)
      {
        std::istringstream strNum( rawString.substr( j+1, endNum - j ) );
        strNum >> tmpValue._variableIndex;
        tmpValue._useVariable = true;
        tmpValue._constElement = rawString.substr( lastPosition, j - lastPosition );
        
        lastPosition = endNum;
      }
      else
        tmpValue._constElement = rawString.substr( lastPosition, j - lastPosition );
    }
    else
        tmpValue._constElement = rawString.substr( lastPosition );

    _fieldElements.push_back( tmpValue );
  }
}

/** returns the string to display
 *  @param[out] outPrintableData : value to print
 *  @param[in]  inForce : force to return the data even if it has not changed
 *  @return true if the value has changed else, the input string will remain unchanged.
 */
bool DeviceMK1::Fields::getPrintableData( std::string &outPrintableData, bool inForce)
{    
  // update the value to print
  if( _changed || inForce )
  {
    _scrollPosition = 0;
    
    _value.clear();
    for( int i = 0; i < _fieldElements.size(); ++i )
    {
      _value += _fieldElements[i]._constElement;
      if( _fieldElements[i]._useVariable )
      {
        _value += _fieldElements[i]._variableValue;
      }
    }
    
    if( _value.size() > _Xlength )
    {
      _scroll = true;
    }
    else
    {
      _scroll = false;
      _printableValue = _value;
      _printableValue.resize( _Xlength, ' ' );
    }
  }
  
  bool ans = _changed || inForce || _scrolled;
  
  if( ans )
  {
    outPrintableData = _printableValue;
    _changed = false;
  }
  
  return ans;
}

}

