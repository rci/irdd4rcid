/** @file irtmanchester_base.h
 *  @brief manchester decoder
 *  This file defines a decoder for manchester codes
 */

#include <list>
#include <rciDaemon/irtranslator_base.h>

#ifndef _IRTMANCHESTER_BASE_H_
#define _IRTMANCHESTER_BASE_H_

namespace Rci
{
class IrManchester_Base : public IrTranslator_Base
{
public:
    IrManchester_Base( std::string inName ) : IrTranslator_Base(inName) {;}
    IrManchester_Base(unsigned int inPulseLength, unsigned int inPulseError,
                      unsigned int inStopLength, std::string inName):
        _pulseLength(inPulseLength), _pulseError(inPulseError), _stopLength(inStopLength), IrTranslator_Base(inName) {;}
    ~IrManchester_Base() {;}
    
    /** translates the prepared data to an unsigned integer corresponding to an IR Code
     *  @param[in] inPreparedData : Data prepared and checked for translation.
     *  @param[in] startPosition : Position at which the translator must start reading
     *  @return the actual state of the translator
     */
    virtual TranslateState translate(const std::vector<IrCode> &inPreparedData, int startPosition = 0) = 0;

    TranslateState manchesterDecode(const std::vector<IrCode> &inPreparedData, int startPosition = 0); ///< decodes manchester data
    
    virtual std::string getName() {return "IrManchester_Base";}
    void setPulseLength(unsigned int inPulseLength) {_pulseLength = inPulseLength;}
    void setPulseError(unsigned int inPulseError) {_pulseError = inPulseError;}
    void setStopLength(unsigned int inStopLength) {_stopLength = inStopLength;}
    
    unsigned int getDataLength(){return _dataLength;}
protected:
    int checkLength(unsigned int inLength) ///< return true if inLength is within _pulseLength +/- _pulseError
        {
            bool ans = false;
            int i;
            // first check for one pulse length then two consecutive pulses
            for( i = 1 ; ( i < 3 ) && ( ans == false ) ; ++i )
            {
                ans = true;
                if(i*(_pulseLength - _pulseError) > inLength)
                    ans = false;
                if(i*(_pulseLength + _pulseError) < inLength)
                    ans = false;
            }
            --i;
            if(ans==false)
                i = -1;
            
            return i;
        }
    
private:
    // parameters
    unsigned int _pulseLength;  ///< base duration of a pulse (us)
    unsigned int _pulseError;   ///< acceptable difference on the pulse length (us)
    unsigned int _stopLength;   ///< length after which the data must be ignored (us)

    // data
    unsigned int _dataLength;   ///< stores the number of _pulseLength readed
};
}
#endif
