/** @file irtTM.h
 *  @brief Time Modulation decoder
 *  This file a decoder for time modulated codes
 */

#include <list>
#include <rciDaemon/irtranslator_base.h>


#ifndef _IRTTM_H_
#define _IRTTM_H_

namespace Rci
{
class IrtTM : public IrTranslator_Base
{
public:
    /** Time modulation translator
      @param[in] inName: name of the translator
      @param[in] in0SL: False State Space duration
      @param[in] in0PL: False State Pulse duration
      @param[in] in1SL: True State Space duration
      @param[in] in1PL: True State Pulse duration
      @param[in] inStopL: End data Pulse duration
      @param[in] inStartL: Start data Pulse duration
      @param[in] nBits: number of bits
      @param[in] lengthError: acceptable error on duration
      @param[in] inLSBFirst: Less Significant bit First (default is false)
      */
    IrtTM( std::string inName,
          uint32_t in0SL, uint32_t in0PL, uint32_t in1SL, uint32_t in1PL,
          uint32_t inStopL, uint32_t inStartL, uint32_t nBits,
          float lengthError, bool inLSBFirst = false):
        IrTranslator_Base(inName), _name(inName), _nBits(nBits), _LSBFirst(inLSBFirst)
        {
          _startLength_min = uint32_t((1.-lengthError)*float(inStartL));
          _startLength_max = uint32_t((1.+lengthError)*float(inStartL));
        
          _FSpaceLength_min = uint32_t((1.-lengthError)*float(in0SL));
          _FSpaceLength_max = uint32_t((1.+lengthError)*float(in0SL));
        
          _FPulseLength_min = uint32_t((1.-lengthError)*float(in0PL));
          _FPulseLength_max = uint32_t((1.+lengthError)*float(in0PL));
       
          _TSpaceLength_min = uint32_t((1.-lengthError)*float(in1SL));
          _TSpaceLength_max = uint32_t((1.+lengthError)*float(in1SL));
        
          _TPulseLength_min = uint32_t((1.-lengthError)*float(in1PL));
          _TPulseLength_max = uint32_t((1.+lengthError)*float(in1PL));
        
          _stopLength_min = uint32_t((1.-lengthError)*float(inStopL));
        }
    
private:
  uint32_t _startLength_min;
  uint32_t _startLength_max;
  uint32_t _FSpaceLength_min;
  uint32_t _FSpaceLength_max;
  uint32_t _FPulseLength_min;
  uint32_t _FPulseLength_max;
  uint32_t _TSpaceLength_min;
  uint32_t _TSpaceLength_max;
  uint32_t _TPulseLength_min;
  uint32_t _TPulseLength_max;
  uint32_t _stopLength_min;
  uint32_t _nBits;
  bool _LSBFirst;         ///< true if the less significant bit is sent first
  std::string _name;

public:
  /** translates the prepared data to an unsigned integer corresponding to an IR Code
   *  @param[in] inPreparedData : Data prepared and checked for translation.
   *  @param[in] startPosition : Position at which the translator must start reading
   *  @return the actual state of the translator
   */
  virtual TranslateState translate(const std::vector<IrCode> &inPreparedData, int startPosition = 0);

  virtual std::string getName() {return _name;}

private:
  int checkStart(const IrCode &inCode) const;
  int findStop(const std::vector<IrCode> &inPreparedData, int inStartPosition, int &outStopPosition) const;
  int mode2ToBit(const std::vector<IrCode> &inPreparedData, int inStartPosition, uint32_t &outCode) const;
  void catBit(bool bit, uint32_t &outCode) const;
};
}

#endif

