#include "irtTM.h"


#ifdef __cplusplus
extern "C" {
#endif
Rci::IrTranslator_Base *maker()   ///< Factory
{
    return new Rci::IrtTM("IrtSIRC", 400, 550, 400, 1000, 12000, 2200, 20, .2, true);
}

void eraser(Rci::IrTranslator_Base *in_Ptr)   ///< Factory
{
    delete in_Ptr;
}
#ifdef __cplusplus
}
#endif

