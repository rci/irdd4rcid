#include "irtTM.h"

namespace Rci
{
class IrtDVD1024 : public IrtTM
{
public:
  IrtDVD1024();
  ~IrtDVD1024();
  
private:
  const uint32_t _hdrPLength_min;
  const uint32_t _hdrPLength_max;
  const uint32_t _hdrSLength_min;
  const uint32_t _hdrSLength_max;
  const uint32_t _ftrP0Length_max;
  const uint32_t _ftrSLength_max;
  const uint32_t _ftrP1Length_max;
  const uint32_t _ftrP0Length_min;
  const uint32_t _ftrSLength_min;
  const uint32_t _ftrP1Length_min;

  /** DVD1024 starts with a starting 8760us pulse followed with a 4240us space.
      Detect the header before sending the other data to the IrtTM translate function
      @return the actual state of the translator
  */
  TranslateState headerRead(const std::vector<IrCode> &inPreparedData, int &startPosition);
  
  /** DVD1024 starts with a starting 8760us pulse followed with a 4240us space.
      Detect the header before sending the other data to the IrtTM translate function
      @return the actual state of the translator
  */
  TranslateState footerRead(const std::vector<IrCode> &inPreparedData, int &startPosition);
  
  /** Long press on a key is coded with a 8760us pulse followed by a 1960us space and a 480us pulse
      @return the actual state of the translator
  */
  TranslateState repeatRead(const std::vector<IrCode> &inPreparedData, int startPosition = 0);
  
  /** translates the prepared data to an unsigned integer corresponding to an IR Code
   *  @param[in] inPreparedData : Data prepared and checked for translation.
   *  @param[in] startPosition : Position at which the translator must start reading
   *  @return the actual state of the translator
   */
  virtual TranslateState translate(const std::vector<IrCode> &inPreparedData, int startPosition = 0);
  
};

}

