#include <sstream>
#include "irtmanchester_base.h"
/** Translates the data to an IR Code integer
 *  @param[in] inPreparedData : IR data to translate
 *  @param[in] startPosition : Position at which the translator must start reading
 *  @return the actual state of the translator.
 */
Rci::IrManchester_Base::TranslateState Rci::IrManchester_Base::manchesterDecode(const std::vector<IrCode> &inPreparedData, int startPosition)
{
    unsigned int itIrCode;
    TranslateState ans = _dataOk;
    int i = 0;
    int nPulses; //number of consecutive pulses
    int totalPulses = 0;
    bool stop = false;
    int checkTime = _pulseLength / 2; // next time at which checking the pulse state.

    _tmpCode = 0;
    
    if(startPosition < inPreparedData.size())
    {
        for(itIrCode = startPosition; ( itIrCode != inPreparedData.size() ) && !stop; ++itIrCode)
        {
            nPulses = checkLength( inPreparedData[itIrCode]._length );
            
            if(nPulses == -1)
            {
                // the length of the first measurement is unknown and untestable so it is skip if invalid
                if(i != 0)
                {
                    if(inPreparedData[itIrCode]._length < _stopLength)
                        ans = _invalidData;
                    else
                        // if a stop length is found, the pulse is kept for the next Manchester translate as first pulse.
                        --i;
                    stop = true;
                }
                else
                    nPulses = 1;
            }
            
            // re-check data
            if(nPulses != -1)
            {
                totalPulses += nPulses;

                if( checkTime < totalPulses * _pulseLength )
                {
                    checkTime += 2*_pulseLength;
                    _tmpCode <<= 1;
                    
                    if(inPreparedData[itIrCode]._pulseState)
                        _tmpCode += 1;
                }
            }
            ++i;
        }
        _stopPosition = i;
        _dataLength = totalPulses;
        
        if( (ans == _dataOk) && !stop)
            // no stop pulse has been detected before the end of the buffer: the buffer has been completely readed
                ans = _bufferEmpty;
    }
    else
    {
        ans = _invalidData;
    }
    
    return ans;
}
