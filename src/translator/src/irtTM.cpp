#include "irtTM.h"

/** Check wether the Mode2 code is a start pulse
 *
 *  @param[in] inCode: Mode2 code to check
 *  @return 0 if inCode is a start pulse, -1 otherwise
 */
int Rci::IrtTM::checkStart(const IrCode &inCode) const
{
  LgCpp::Logger logger("IrtTM checkStart");
  int ans = -1;
  if(!inCode._pulseState)
  {
    if((inCode._length > _startLength_min) & (inCode._length < _startLength_max))
    {
      ans = 0;
    }
    else
    {
      logger.logIt( "Incorrect Length" , LgCpp::Logger::logDEBUG);
    }
  }
  else
  {
    logger.logIt("Incorrect State", LgCpp::Logger::logDEBUG);
  }
  if(ans < 0)
  {
    std::ostringstream ssMessage;
    ssMessage << "code: " << _startLength_min << " < " << inCode._length
              << " < " << _startLength_max;
    logger.logIt(ssMessage, LgCpp::Logger::logDEBUG);
  }
  return ans;
}

/** Find a stop code in the input data
 *
 *  @param[in] inPreparedData: Mode2 code list
 *  @param[in] inStartPosition: Start index for the search process
 *  @param[out] outStopPosition: Index of the stop space if found
 *  @return 0 if a stop was found, -1 otherwise
 */
int Rci::IrtTM::findStop
  (const std::vector<IrCode> &inPreparedData, int inStartPosition, int &outStopPosition) const
{
  int ans = -1;
  
  for( int i = inStartPosition; (i < inPreparedData.size()) && (ans == -1); ++i )
  {
    if(inPreparedData[i]._pulseState)
      if(inPreparedData[i]._length > _stopLength_min)
      {
        uint32_t tst = _stopLength_min;
        outStopPosition = i;
        ans = 0;
      }
  }
  return ans;
}

/** Convert consecutive pluse and space in the corresponding bit state and add it to the 
 *
 *  @param[in] inPreparedData: Mode2 code list
 *  @param[in] inStartPosition: Start index for the search process
 *  @param[out] outCode: Modified code
 *  @return 0 the two Mode2 codes were correctly converted, -1 otherwise
 */
int Rci::IrtTM::mode2ToBit
  (const std::vector<IrCode> &inPreparedData, int inStartPosition, uint32_t &outCode) const
{
  bool isTrue = 0;
  bool isFalse = 0;
  int ans = -1;
  LgCpp::Logger logger("IrtTM mode2ToBit");
  
  if(inStartPosition + 1 < inPreparedData.size())
  {
    const IrCode &spaceCode = inPreparedData[inStartPosition];
    const IrCode &pulseCode = inPreparedData[inStartPosition + 1];
    if(spaceCode._pulseState && (pulseCode._pulseState == false))
    {
      isFalse = ((spaceCode._length < _FSpaceLength_max) && (spaceCode._length > _FSpaceLength_min))?
        (((pulseCode._length < _FPulseLength_max) && (pulseCode._length > _FPulseLength_min))? true : false)
        : false;
      isTrue = ((spaceCode._length < _TSpaceLength_max) && (spaceCode._length > _TSpaceLength_min))?
        (((pulseCode._length < _TPulseLength_max) && (pulseCode._length > _TPulseLength_min))? true : false)
        : false;
      if(isTrue != isFalse)
      {
        catBit(isTrue, outCode);
        ans = 0;
      }
    }
    
    if(ans < 0)
    {
        std::ostringstream ssMessage;
        ssMessage << "isFalse: " << isFalse << " isTrue: " << isTrue << std::endl;
        ssMessage << "spaceLength: " << spaceCode._length 
                  << " pulseLength: " << pulseCode._length;
        logger.logIt(ssMessage, LgCpp::Logger::logDEBUG);
    }
  }
  else
  {
    std::ostringstream ssMessage;
    ssMessage << "StartPosition: " << inStartPosition 
              << " dataLength: " << inPreparedData.size() << std::endl;
    logger.logIt(ssMessage, LgCpp::Logger::logDEBUG);
  }
  
  return ans;
}

/** Add bit to the code
 *
 *  @param[in] inBit: bit to add
 *  @param[out] outCode: Modified code
 */
void Rci::IrtTM::catBit(bool bit, uint32_t &outCode) const
{
  if(_LSBFirst)
  {
    outCode >>=1;
    outCode += bit? 0x80000000 : 0;
  }
  else
  {
    outCode <<=1;
    outCode += bit? 1 : 0;
  }
}

Rci::IrtTM::TranslateState Rci::IrtTM::translate(const std::vector<IrCode> &inPreparedData,
                                                 int startPosition)
{
  unsigned int itIrCode = startPosition;
  TranslateState ans = _dataOk;
  if(startPosition < inPreparedData.size())
  {
    int nData = 0;
    // skip the first element if it is a space
    if(inPreparedData[itIrCode]._pulseState)
      ++itIrCode;
    if(itIrCode < inPreparedData.size())
    {
      // check if a start pulse is found
      if(checkStart(inPreparedData[itIrCode]) >= 0)
      {
        int itStart = itIrCode + 1;
        int itStop = itIrCode + 2 * _nBits;
        int ok = 0;
        if( inPreparedData.size() - itStart >= 2 * _nBits )
        {
          uint32_t code = 0;
          for(itIrCode = itStart; (itIrCode < itStop) && (ok >= 0); itIrCode += 2)
            ok = mode2ToBit(inPreparedData, itIrCode, code);
          if(ok < 0)
          {
            _logger.logIt("could not translate data", LgCpp::Logger::logDEBUG);
            _stopPosition = itIrCode;
            ans = _invalidData;
          }
          else
          {
            if(_LSBFirst)
              code >>= 32 - _nBits;
            std::ostringstream ssCode;
            ssCode << code;
            _code = ssCode.str();
            _stopPosition = itStart + 2 * _nBits;
            ans = _dataOk;
          }
        }
        else
        {
          // not enough data in the buffer
          ans = _bufferEmpty;
        }
      }
      else
      {
        // no start pulse found
        _stopPosition = itIrCode + 1;
        ans = _invalidData;
      }
    }
    else
    {
      // not enough data
      ans = _bufferEmpty;
    }
  }
  else
  {
    ans = _bufferEmpty;
  }
  return ans;
}

