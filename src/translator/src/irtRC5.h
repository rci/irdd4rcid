/** @file irtRC5.h
 *  @brief Philips RC5 decoder
 *  This file defines an RC5 decoder for Infra Red philips remote commands.
 */

#include <sstream>
#include <list>
#include "irtmanchester_base.h"

#ifndef _IRTRC5_H_
#define _IRTRC5_H_

namespace Rci
{

class IrTRC5 : public IrManchester_Base
{
public:
    static IrTranslator_Base *maker();    ///< Factory
    static void eraser( IrTranslator_Base *in_Ptr );  ///< Factory
    
    IrTRC5() : IrManchester_Base(800, 200, 8000, "RC5-Translator"), _numberOfPulses(28) {;}
    ~IrTRC5() {;}
    
    TranslateState translate(const std::vector<IrCode> &inPreparedData, int startPosition = 0);

    std::string getName() {return "IrtRC5";}

private:
  unsigned int _numberOfPulses;
  bool _repressFlag; ///< this flag corresponds to the twelveth bit of the data
  void setCode(unsigned int inCode)
  {
    bool tmpRepressFlag = false;
    if(inCode & 0x800)
      tmpRepressFlag = true;
    
    _oldCode = _code;
    
    std::ostringstream ssCode("");
    ssCode << (inCode & 0xFFFFF7FF);

    _code = ssCode.str(); // clears the repressed Flag
    

    if( ( _oldCode != _code ) || ( _repressFlag != tmpRepressFlag ) )
    {
      _repeated = false;
      _repressFlag = tmpRepressFlag;
    }
    else
      _repeated = true;
  }
};
}
#endif
