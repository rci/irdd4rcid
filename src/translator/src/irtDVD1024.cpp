#include "irtDVD1024.h"

#ifdef __cplusplus
extern "C" {
#endif

Rci::IrtDVD1024::IrtDVD1024():
  Rci::IrtTM("IrtDVD1024", 376, 500, 1500, 500, 38000, 392, 32, .1, false),
  _hdrPLength_min(8700), _hdrPLength_max(8820), _hdrSLength_min(4180), _hdrSLength_max(4300),
  _ftrP0Length_min(8700), _ftrP0Length_max(8820), _ftrSLength_min(1920), _ftrSLength_max(2000),
  _ftrP1Length_min(440), _ftrP1Length_max(530)
  {;}

Rci::IrtDVD1024::~IrtDVD1024() {;}

Rci::IrtDVD1024::TranslateState
    Rci::IrtDVD1024::headerRead(const std::vector<IrCode> &inPreparedData, int &startPosition)
{
  int length = inPreparedData.size() - startPosition;
  TranslateState ans = _invalidData;
  if(length >= 3)
  {
    // skip the first element if it is a space
    if(inPreparedData[startPosition]._pulseState)
      ++startPosition;
    int hdrP = inPreparedData[startPosition]._length;
    ++startPosition;
    int hdrS = inPreparedData[startPosition]._length;
    ++startPosition;
    
    if(  ( _hdrPLength_min < hdrP ) && ( hdrP < _hdrPLength_max )
      && ( _hdrSLength_min < hdrS ) && ( hdrS < _hdrSLength_max ))
      ans = _dataOk;
  }
  else
    ans = _bufferEmpty;
  return ans;
}

Rci::IrtDVD1024::TranslateState
    Rci::IrtDVD1024::footerRead(const std::vector<IrCode> &inPreparedData, int &startPosition)
{
  int length = inPreparedData.size() - startPosition;
  TranslateState ans = _invalidData;
  if( length > 2 )
  {
    // skip the first element if it is a space
    int ftrP0 = inPreparedData[startPosition]._length;
    ++startPosition;
    int ftrS =  inPreparedData[startPosition]._length;
    ++startPosition;
    int ftrP1 = inPreparedData[startPosition]._length;
    
    if(  ( _ftrP0Length_min < ftrP0 ) && ( ftrP0 < _ftrP0Length_max )
      && ( _ftrSLength_min  < ftrS  ) && ( ftrS  < _ftrSLength_max  )
      && ( _ftrP1Length_min < ftrP1 ) && ( ftrP1 < _ftrP1Length_max ))
      ans = _dataOk;
  }
  else
    ans = _bufferEmpty;
  return ans;
}

Rci::IrtDVD1024::TranslateState
    Rci::IrtDVD1024::repeatRead(const std::vector<IrCode> &inPreparedData, int startPosition)
{
}

Rci::IrtDVD1024::TranslateState
    Rci::IrtDVD1024::translate(const std::vector<IrCode> &inPreparedData, int startPosition)
{
  int length = inPreparedData.size() - startPosition;
  int itIrCode = startPosition;

  TranslateState ans = headerRead(inPreparedData, itIrCode);

  if( ans == _dataOk )
  {
    ans = Rci::IrtTM::translate(inPreparedData, itIrCode);
    
    if( ans == _dataOk )
    {
      itIrCode += 66;
      ans = footerRead(inPreparedData, itIrCode);
      _stopPosition = itIrCode + 1;
      
      return ans;
    }
  }
  else
    return ans;
}
  

Rci::IrTranslator_Base *maker()   ///< Factory
{
    return new Rci::IrtDVD1024();
}

void eraser(Rci::IrTranslator_Base *in_Ptr)   ///< Factory
{
    delete in_Ptr;
}
#ifdef __cplusplus
}
#endif

