
#include "irtRC5.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::IrTranslator_Base *maker()   ///< Factory
{
    return Rci::IrTRC5::maker();
}

void eraser(Rci::IrTranslator_Base *in_Ptr)   ///< Factory
{
    Rci::IrTRC5::eraser(in_Ptr);
}
#ifdef __cplusplus
}
#endif

Rci::IrTranslator_Base *Rci::IrTRC5::maker()   ///< Factory
{
    Rci::IrTranslator_Base *ans = new Rci::IrTRC5;
    return ans;
}

void Rci::IrTRC5::eraser(Rci::IrTranslator_Base *in_Ptr)   ///< Factory
{
    delete in_Ptr;
}


/** Translates the data to an IR Code integer
 *  @param[in] inPreparedData : IR data to translate
 *  @param[in] startPosition : position in the data at which the translation must start
 *  @return State of the translation
 */
Rci::IrTRC5::TranslateState Rci::IrTRC5::translate(const std::vector<IrCode> &inPreparedData, int startPosition)
{
    TranslateState ans = manchesterDecode(inPreparedData, startPosition);

    switch( ans )
    {
      case _invalidData:
        break;
      case _bufferEmpty:
      case _dataOk:
        if( ( _numberOfPulses == getDataLength() ) || ( _numberOfPulses - 1 == getDataLength() ) )
            ans = _dataOk;
        else
        {
            if( _numberOfPulses < getDataLength() )
                // the data is already too long
                ans = _invalidData;
            else if( _numberOfPulses > getDataLength() + 1 )
            {
                if( ans == _dataOk )
                    // the end of the data arrived to fast.
                    ans = _invalidData;
                else
                    ans = _bufferEmpty;
            }
        }
        break;
      default:
        break;
    }

    if(ans == _dataOk)
      setCode( _tmpCode );
    
    return ans;
}
