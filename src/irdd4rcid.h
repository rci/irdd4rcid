#ifndef _HCIDEVICEMK1_H_
#define _HCIDEVICEMK1_H_

#include <string>
#include <map>
#include <vector>
#include <logcplusplus.hpp>
#include <confuse.h>
#include <irddriver/ir-display-driver.h>
#include <rciDaemon/rciDeviceBase.h>

#include "fields.h"

#ifndef MAX_SCROLL_DELAY
#define MAX_SCROLL_DELAY 10
#endif
namespace Rci
{
/** This class defines the screen sizes
 */
class IrdDevice :
  public RciDeviceBase,
  public IrDD
{
private:
  unsigned int _nLinesDisplayed;  ///< Number of line that the screen can display
  unsigned int _nLinesStored;     ///< Number of line that the screen can store
  unsigned int _nCharsDisplayed;  ///< Number of character per line that the screen can display
  unsigned int _nCharsStored;     ///< Number of character per line that the screen can store
  
  std::vector<unsigned int>   _lineStart; ///< Starting addresses of the first character of each line

  std::string _devicePath;
  bool _lightState; ///< backlight state
  bool _force;
  bool _firstRead;    ///< true at construction to ensure that _itPage will be correctly initialized
  std::string  _lastRequestedPage;
  boost::mutex _irMutex;
  boost::mutex _pageMutex;
  int _scrollSpeed;
  boost::asio::deadline_timer _scrollTimer;
  boost::asio::deadline_timer _backlightTimer;

  std::map<std::string, std::vector<DeviceMK1::Fields> > _availableOutpages; ///< fields to display   
  std::map<std::string, std::vector<DeviceMK1::Fields> >::iterator _itPage;  ///< current page to display   
  
  std::map<RcCode, std::string> _actionsMap; ///< actions for the device
  
  int setCursorPosition(unsigned int inLine, unsigned int inPosition);
  void backlightState(bool inOnOff); ///< switch the backlight on/off
  void scrollFields(const boost::system::error_code&);
  void displayAllFields( bool scroll = false ); ///< display all fileds in _itPage
  int displayField( const std::string &inDataToDisplay, unsigned int inLine, unsigned int inStart ); ///< echo the data on the correct position on the screen
  
  timeval _IRDataLifeTime; ///< IR Data life time
  timeval _IRDataUpdate;   ///< time of the last update
  
  void irCodeHandler(){addIRData(getCurrentIrData());}
  void setError(const boost::system::error_code&);
  void onOpen();
  
  void addIRData(IrDD::IrMode2 inIRData); ///< add the data to the received IR Data

  int removeIRData(int inNData); ///< removes the N first data from _IR_Data
protected:
  int setNLinesDisplayed(unsigned int inLines);   ///< sets the maximum number of line displayed by the screen
  int setNLinesStored(unsigned int inLines);  ///< sets the maximum number of lines stored in the screen memory
  int setNCharDisplayed(unsigned int inChars);///< sets the maximum number of characters displayed by the screen on one line
  int setNCharsStored(unsigned int inChars);  ///< sets the maximum number of characters stored on each line.
  int setLineStart(const std::vector<unsigned int> &inLineStart); ///< returns the number of lines readed, or -1 if the input vector was to short.
  int setLineStart(unsigned int inAddress, unsigned int inLineNumber); ///< set the start address of the corresponding line
  int getLineStart(unsigned int &outAddress, unsigned int inLineNumber); ///< returns the start address of the corresponding line
  unsigned int getNLinesDisplayed(){return _nLinesDisplayed;} ///< returns the maximum number of line displayed by the screen
  unsigned int getNLinesStored(){return _nLinesStored;}       ///< returns the maximum number of lines stored in the screen memory
  unsigned int getNCharDisplayed(){return _nCharsDisplayed;}  ///< returns the maximum number of characters displayed by the screen on one line
  unsigned int getNCharsStored(){return _nCharsStored;}       ///< returns the maximum number of characters stored on each line.

public:
  IrdDevice(const std::string &inPath);
  ~IrdDevice();
  
  int init(const std::string &inConfigPath);
  int echoPage(const std::list<RciOutpage> &inDataToDisplay, const std::string &inOutpageName);
  int commandScreen(ScreenCommands inCommand);///< changes the screen behaviour

  int translate( RcCode &outCmd ); ///< returns a command and the name of the translator
  void setAnswer( std::string const&, Rci::CmdAnswer const&, Rci::DeviceSpecificData*) {;}
};

}

#endif
