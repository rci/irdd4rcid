#include <vector>
#include <sstream>
#include <sys/time.h>
#include "irdd4rcid.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::RciDeviceBase *maker(const std::string &inPath)   ///< Factory
{
  return new Rci::IrdDevice(inPath);
}

void eraser(Rci::RciDeviceBase *in_Ptr)   ///< Factory
{
  delete in_Ptr;
}
#ifdef __cplusplus
}
#endif


///\todo handle errors
/** @param[in] inPath : path to the device to open
*/
Rci::IrdDevice::IrdDevice(const std::string &inPath) : RciDeviceBase(inPath, "IrdDevice"),
    _force(false), _firstRead(true), _scrollTimer(_io), _backlightTimer(_io),
    _scrollSpeed(MAX_SCROLL_DELAY), _nLinesStored(0), _nLinesDisplayed(0), _nCharsDisplayed(0),
    _nCharsStored(0)
{
  IrDD::_logger.logIt("making device", LgCpp::Logger::logINFO);

  _IRDataLifeTime.tv_sec = 10;
  _IRDataLifeTime.tv_usec = 0;
  _IRDataUpdate.tv_sec = 0;
  _IRDataUpdate.tv_usec = 0;
  
  std::vector<DeviceMK1::Fields> empty;
  _availableOutpages["empty"] = empty;
  _itPage = _availableOutpages.begin();
  
  if( init( inPath ) >= 0 );
    open(_devicePath);

  setNLinesStored(2);
  setNLinesDisplayed(2);
  setNCharsStored(40);
  setNCharDisplayed(16);
  
  std::vector<unsigned int> startLines( getNLinesStored(), 0 );
  startLines.resize( getNLinesStored() );
  startLines[0] = 0x0;
  startLines[1] = 0x40;
  
  setLineStart(startLines);
  
  backlightState(true);

  IrDD::_logger.logIt("making device ended", LgCpp::Logger::logINFO);
}

Rci::IrdDevice::~IrdDevice()
{}

/** Sets the number of lines that the screen can display. Obviously, this number cannot exceed the number of lines that
 *  the memory of the screen can store
 *  @param[in] inLines : Number of lines to put
 *  @return 0 on success, -1 if inLines exceed the maximum number of lines that the screen can store
 */
int Rci::IrdDevice::setNLinesDisplayed(unsigned int inLines)
{
    int ans = 0;
    if(inLines <= _nLinesStored)
        _nLinesDisplayed = inLines;
    else
        ans = -1;
    return ans;
}

/** Sets the number of lines that the screen can store. The vector _lineStart is resized with the input value
 *  @param[in] inLines : Number of lines to put
 *  @return always 0
 */
int Rci::IrdDevice::setNLinesStored(unsigned int inLines)
{
    _nLinesStored = inLines;
    _lineStart.resize(inLines);
    return 0;
}

/** Sets the number of characters that the screen can display. Obviously, this number cannot exceed the number of
 *  characters that the memory of the screen can store
 *  @param[in] inChars : Number of characters to put
 *  @return 0 on success, -1 if inChars exceed the maximum number of characters that the screen can store
 */
int Rci::IrdDevice::setNCharDisplayed(unsigned int inChars)
{
    int ans = 0;
    if(inChars <= _nCharsStored)
        _nCharsDisplayed = inChars;
    else
        ans = -1;
    return ans;
}

/** Sets the number of characters that the screen can store.
 *  @param[in] inChars : Number of characters to put
 *  @return always 0
 */
int Rci::IrdDevice::setNCharsStored(unsigned int inChars)
{
    _nCharsStored = inChars;
    return 0;
}

/** Sets the address of each line
 *  @param[in] inLineStarts : address to put
 *  @return number of address readed, -1 if the input vector is too short
 */
int Rci::IrdDevice::setLineStart(const std::vector<unsigned int> &inLineStarts)
{
    int ans;
    if(inLineStarts.size() < _lineStart.size())
        ans = -1;
    else
    {
        for(ans = 0; ( ans < inLineStarts.size() ) && ( ans < _lineStart.size() ); ++ans)
        {
            _lineStart[ans] = inLineStarts[ans];
        }
    }
    return ans;
}

/** Sets the address of the corresponding line number
 *  @param[in] inAddress : address to put
 *  @param[in] inLineNumber : line concerned
 *  @return 0 on success, -1 on failure
 */
int Rci::IrdDevice::setLineStart(unsigned int inAddress, unsigned int inLineNumber)
{
    int ans = 0;
    if(inLineNumber < _lineStart.size())
    {
        _lineStart[inLineNumber] = inAddress;
    }
    else
        ans = -1;
    return ans;
}
 
/** Sets the address of the corresponding line number
 *  @param[out] outAddress : address to put
 *  @param[in] inLineNumber : line concerned
 *  @return 0 on success, -1 on failure
 */
int Rci::IrdDevice::getLineStart(unsigned int &outAddress, unsigned int inLineNumber)
{
    int ans = 0;
    if(inLineNumber < _lineStart.size())
    {
        outAddress = _lineStart[inLineNumber];
    }
    else
        ans = -1;
    return ans;
}

/** initialize the device with the input config file
 *  @param[in] inConfigPath : configuration file to use
 */
int Rci::IrdDevice::init(const std::string &inConfigPath)
{
  int ans = 0;

  static cfg_opt_t fieldOpt[] =
  {
    CFG_INT("Xstart" ,         0, CFGF_NONE),
    CFG_INT("Ystart" ,         0, CFGF_NONE),
    CFG_INT("Xlength",         0, CFGF_NONE),
    CFG_END()
  };

  static cfg_opt_t pageOpt[] =
  {
    CFG_SEC("field",   fieldOpt, CFGF_TITLE | CFGF_MULTI),
    CFG_END()
  };
  
  static cfg_opt_t commandOpt[] =
  {
    CFG_STR_LIST("arguments", "", CFGF_NONE),
    CFG_END()
  };

  static cfg_opt_t translatorOpt[] =
  {
    CFG_SEC("command", commandOpt, CFGF_TITLE | CFGF_MULTI),
    CFG_END()
  };

  static cfg_opt_t actionOpt[] =
  {
    CFG_SEC("translator", translatorOpt, CFGF_TITLE | CFGF_MULTI),
    CFG_END()
  };
 
  cfg_opt_t deviceOpt[] =
  {
    CFG_STR("devicePath"       ,              "", CFGF_NONE),
    CFG_INT("scrollSpeed"      ,MAX_SCROLL_DELAY, CFGF_NONE),
    CFG_SEC("page"             ,         pageOpt, CFGF_TITLE | CFGF_MULTI),
    CFG_SEC("action"           ,       actionOpt, CFGF_TITLE | CFGF_MULTI),
    CFG_END()
  };

  cfg_t *cfg;
  cfg = cfg_init(deviceOpt, CFGF_NONE);
  
  std::ostringstream ssMessage;
  
  if( cfg_parse(cfg, inConfigPath.c_str()) == CFG_PARSE_ERROR )
  {
    ssMessage.str("");
    ssMessage << "error n°" << errno << " opening " << inConfigPath;
    IrDD::_logger.logIt(ssMessage, LgCpp::Logger::logERROR);
    ans = -1;
  }
  else
  {
    _devicePath = cfg_getstr( cfg, "devicePath" );
    _scrollSpeed = cfg_getint(cfg, "scrollSpeed");
    _scrollSpeed = (_scrollSpeed > MAX_SCROLL_DELAY)? _scrollSpeed : MAX_SCROLL_DELAY;

    cfg_t *cfgPage;

    boost::unique_lock<boost::mutex> lk(_pageMutex);
    for( int iPage = 0; iPage < cfg_size( cfg, "page"); ++iPage )
    {
      cfgPage = cfg_getnsec( cfg, "page", iPage );
      _availableOutpages[ cfg_title(  cfgPage ) ].resize( cfg_size( cfgPage, "field") );
  
      cfg_t *cfgField;
      for( int iField = 0; iField < cfg_size( cfgPage, "field"); ++iField )
      {
        cfgField = cfg_getnsec( cfgPage, "field", iField );


        Rci::DeviceMK1::Fields page( cfg_getint( cfgField, "Xstart"),
                                     cfg_getint( cfgField, "Xlength"),
                                     cfg_getint( cfgField, "Ystart") );
        
        std::string tmpString = cfg_title(  cfgField );
        page.makeElements( tmpString );

        _availableOutpages[ cfg_title(  cfgPage ) ][iField] = page;
      }
    }
    
    cfg_t *cfgAction, *cfgTranslator, *cfgCommand;
    for(int iAction = 0; iAction < cfg_size( cfg, "action"); iAction ++)
    {
      cfgAction = cfg_getnsec( cfg, "action", iAction );
      for( int iTranslator = 0; iTranslator < cfg_size( cfgAction, "translator"); ++iTranslator )
      {
        cfgTranslator = cfg_getnsec( cfgAction, "translator", iTranslator );
        
        for( int iCommand = 0; iCommand < cfg_size( cfgTranslator, "command"); ++iCommand )
        {
          cfgCommand = cfg_getnsec( cfgTranslator, "command", iCommand );
          CmdArguments arguments;
          // gets the default arguments for command.
          for( int iArgument = 0; iArgument < cfg_size( cfgCommand, "arguments"); ++iArgument)
          {
            std::string argument = cfg_getnstr(cfgCommand, "arguments", iArgument);
            arguments.push_back( argument );
          }
          RcCode rcCode( cfg_title( cfgCommand ), cfg_title( cfgTranslator ), arguments );
          _actionsMap.insert( std::pair<RcCode, std::string>( rcCode, cfg_title( cfgAction ) ) );
        }
      }
    }
  }
  cfg_free(cfg);
  
  boost::system::error_code e;
  scrollFields(e);
  
  IrDD::_logger.logIt("loaded", LgCpp::Logger::logINFO);
  
  return ans;
}

/** @param[in] inDataToDisplay : value to print on the screen
 *  @param[in] inLine : Line on which the data must be displayed
 *  @param[in] inStart : position of the first character of the data
 *  @return number of characters written on success, -1 on failure
 */
int Rci::IrdDevice::displayField( const std::string &inDataToDisplay, unsigned int inLine,
                                   unsigned int inStart )
{
  int ans = 0;

  if( setCursorPosition(inLine, inStart) < 0)
    ans = -1;
  else
  {
    if( inDataToDisplay.size() > 0)
    {
      // Makes a command to send the data
      IrDD_Cmd deviceCommand;
      deviceCommand.set_Cmd(CMD_CHAR_TO_SCREEN);
      deviceCommand.add_ASCII_To_Parameters(inDataToDisplay);
      
      sendCmd(deviceCommand);
    }
  }
  return ans;
}

/** Sends the data to the device.
 *  @param[in] inDataToDisplay : list of data available for display
 *  @param[in] inOutpageName : name of the outpage to use
 */
int Rci::IrdDevice::echoPage(const std::list<RciOutpage> &inDataToDisplay,
                                const std::string &inOutpageName)
{
  int ans = 0;
  boost::unique_lock<boost::mutex> lk(_pageMutex);
  
  _force = false;
  
  if( inOutpageName != _lastRequestedPage || _firstRead )
  {
    _lastRequestedPage = inOutpageName;
    
    _force = true;
    _firstRead = false;
    
    _itPage = _availableOutpages.find(inOutpageName);
    if( _itPage == _availableOutpages.end() )
      _itPage = _availableOutpages.begin();
  }
  
  std::vector<DeviceMK1::Fields>::iterator itField;
  std::list<RciOutpage>::const_iterator itData;
  std::string value;
  
  int iParam = 0;
  {
    for( itData = inDataToDisplay.begin() ; itData != inDataToDisplay.end(); ++itData )
    {
      // checks if the data from the plugin has changed
      if( itData->constGetChanged() )
      {
        value = itData->getText();
        for( itField = _itPage->second.begin(); itField != _itPage->second.end(); ++itField )
          itField->setValue( value, iParam );
      }        
      ++iParam;
    }
  }

  displayAllFields();
  
  return ans;
}

void Rci::IrdDevice::displayAllFields(bool scroll)
{
  std::vector<DeviceMK1::Fields>::iterator itField;
  std::string value;
  if( _itPage != _availableOutpages.end() )
  {
    for( itField = _itPage->second.begin(); itField != _itPage->second.end(); ++itField )
    {
      if(scroll)
        itField->scroll();
      // checks if the data to display has changed
      if( itField->getPrintableData( value, _force ) )
        displayField( value, itField->getYposition(), itField->getXposition() );
    }
  }
}

void Rci::IrdDevice::scrollFields(const boost::system::error_code& e)
{
  if(!e)
  {
    boost::unique_lock<boost::mutex> lk(_pageMutex);
    displayAllFields(true);
    _scrollTimer.expires_from_now( boost::posix_time::millisec(_scrollSpeed) );
    _scrollTimer.async_wait(boost::bind(&Rci::IrdDevice::scrollFields, this, _1));
  }
}

/** sets the cursor to the correct position on the screen
 *  @param[in] inLine : line where to put the cursor
 *  @param[in] inPosition : character where to put the cursor
 *  @return 0 on success, -1 on failure
 */
int Rci::IrdDevice::setCursorPosition(unsigned int inLine, unsigned int inPosition)
{
  int ans = 0;
  unsigned int startAddress;

  if(getLineStart(startAddress, inLine) < 0)
    ///\todo handle error line out of range
    ans = -1;
  else
  {
    IrDD_Cmd deviceCommand;
    std::vector<IrDDByte> deviceParams;
    IrDDByte bBValue;
                
    if(inPosition < getNCharsStored())
    {
      // calculate the starting position
      startAddress += inPosition;
      
      deviceCommand.set_Cmd(CMD_CMD_TO_SCREEN);
      
      // casts the start address to a Byte
      bBValue = startAddress;

      // selects the «position cursor mode»
      ///\todo : create constant masks for these functions.
      bBValue |= 0x80;
      deviceParams.push_back(bBValue);
      deviceCommand.add_Bytes_To_Parameters(deviceParams);
      
      // sets the cursor position
      ans = sendCmd(deviceCommand);
    }
    else
      ///\todo handle error start out of range
      ans = -1;
  }

  return ans;
}

/** switch the light on or off
 *  @param[in] inOnOff : True to switch the light on.
 */
void Rci::IrdDevice::backlightState(bool inOnOff)
{
  IrDD_Cmd cmd;
  if( _backlightTimer.expires_from_now() <= boost::posix_time::millisec(0) )
  {
    _backlightTimer.expires_from_now(  boost::posix_time::millisec(500) );
    _lightState = inOnOff;
    if( _lightState )
    {
      cmd.set_Cmd(CMD_BCK_LIGHT_ON);
      IrDD::_logger.logIt("light state : on", LgCpp::Logger::logINFO);
    }
    else
    {
      cmd.set_Cmd(CMD_BCK_LIGHT_OFF);
      IrDD::_logger.logIt("light state : off", LgCpp::Logger::logINFO);
    }
    sendCmd(cmd);
  }
}

/** sends a command to the screen
 *  @param[in] inCommand : command to send
 *  @return 0 on success, -1 on failure
 */
int Rci::IrdDevice::commandScreen(ScreenCommands inCommand)
{
  int ans = 0;
  
  IrDD_Cmd deviceCommand;
  std::vector<IrDDByte> deviceParams;
  
  switch(inCommand)
  {
    case _clear :
      deviceCommand.set_Cmd(CMD_CMD_TO_SCREEN);
      deviceParams.push_back(0x01);   // clears the screen
      deviceCommand.add_Bytes_To_Parameters(deviceParams);
      ans = sendCmd(deviceCommand);
  }
  if( ans == -1 )
  {
    //todo handle error
  }
  return ans;
}

void Rci::IrdDevice::setError(const boost::system::error_code& inError)
{
  std::ostringstream ssMessage;
  ssMessage << inError << " : " << inError.message();
  if( inError != getError() )
    IrDD::_logger.logIt(ssMessage, LgCpp::Logger::logERROR);
  IrDD::setError(inError);
}

void Rci::IrdDevice::onOpen()
{
  _force = true;
}

/** Adds the data to the list of received data.
 *  @param[in] inData : data to add to the list.
 */
void Rci::IrdDevice::addIRData(IrDD::IrMode2 inData)
{
  boost::unique_lock<boost::mutex> lk(_irMutex);
  _IR_Data.push_back(inData);
  RcCode code;
  if(translate(code) == 0)
  {
    std::ostringstream ssMessage;
    ssMessage << "received IRCODE : " << code.getCode();
    IrDD::_logger.logIt( ssMessage, LgCpp::Logger::logINFO );
    std::map<RcCode, std::string>::iterator it = _actionsMap.find(code);
    if( it != _actionsMap.end() )
    {
      if(it->second == "backlight")
        backlightState(!_lightState);
    }
    else
      sendCommand(code);
  }
}

/** Removes the N first data of the IR Data buffer
 *  @param[in] inNData : Number of Data to remove
 *  @return number of data removed.
 */
int Rci::IrdDevice::removeIRData(int inNData)
{
  int ans = 0;
  for (ans = 0; ( ans < inNData ) && (_IR_Data.begin() != _IR_Data.end() ); ++ans )
    _IR_Data.pop_front();
  return ans;
}

/** @param[out] outCmd : command returned by the translator
 *  @return 0 on success, -1 on error
 */
int Rci::IrdDevice::translate( RcCode &outCmd )
{
  int ans = -1;
  unsigned int nCodeToRead = 10;
    
  // Get the last received IR data.
  if( _IR_Data.size() > 0 )
  {
    std::vector<Rci::IrTranslator_Base::IrCode> preparedData;
    int ansChkData = Rci::IrTranslator_Base::checkAndPrepareData( _IR_Data, preparedData );
    
    if( ansChkData >= 0 )
    {
      // State of the whole translators
      Rci::IrTranslator_Base::TranslateState trState = Rci::IrTranslator_Base::_invalidData;
      
      // Position of the last data readed when invalid
      unsigned int invalidPosition = 0;
      unsigned int readPosition = 0;
      unsigned int nElementReaded = 0;
      int validTranslatorIndex = 0;
      
      RcCode tmpCmd;        
      // check every translator for the incomming data
      for ( int i = 0; ( i < _vecTranslators.size() ); ++i &&
        ( ans != Rci::IrTranslator_Base::_dataOk ) )
      { 
        
        Rci::IrTranslator_Base::TranslateState tmpTrState = (*(_vecTranslators[i])).translate(
                                                                  preparedData, nCodeToRead, tmpCmd);
                                                                  
        nElementReaded = tmpCmd.getNumberOfIRDataReaded();
        switch( tmpTrState )
        {
          case Rci::IrTranslator_Base::_dataOk:
            trState = tmpTrState;
            validTranslatorIndex = i;
            readPosition = nElementReaded;

            outCmd.setTranslatorName( tmpCmd.getTranslatorName() );
            outCmd.setCode( tmpCmd.getCode() );
            outCmd.setNumberOfConsecutive( tmpCmd.getNumberOfConsecutive() );
            outCmd.setIsNew( tmpCmd.getIsNew() );
            outCmd.setNumberOfIRDataReaded( tmpCmd.getNumberOfIRDataReaded() );

            break;
          case Rci::IrTranslator_Base::_invalidData:
            // this will be used to remove the less invalid data as possible if all translators return
            // _invalidData
            if(invalidPosition == 0 || nElementReaded < invalidPosition)
              if (nElementReaded > 0)
                invalidPosition = nElementReaded;
            break;
          case Rci::IrTranslator_Base::_bufferEmpty:
            trState = tmpTrState;
            break;
        }
      }
        
      // no translator could read the data
      std::ostringstream ssInvMessage;
      switch( trState )
      {
        case Rci::IrTranslator_Base::_invalidData:
          ssInvMessage << "removing " << invalidPosition << " invalid elements";
          // Ensures that at least one invalid data is removed
          if (invalidPosition == 0)
            invalidPosition = 1;
          removeIRData(invalidPosition);
          IrDD::_logger.logIt(ssInvMessage, LgCpp::Logger::logDEBUG);
          break;
        case Rci::IrTranslator_Base::_bufferEmpty:
          break;
        case Rci::IrTranslator_Base::_dataOk:                    
          removeIRData(readPosition);
          ans = 0;
          break;
      }
    }
    else
    {
      // check data failed
      removeIRData( -ansChkData );
    }
  }
  return ans;
}

