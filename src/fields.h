#include <string>
#include <vector>

#ifndef _DEVICEMK1FIELDS_H_
#define _DEVICEMK1FIELDS_H_
namespace Rci
{
namespace DeviceMK1
{

struct FieldStringElements
{
public:
  FieldStringElements() : _variableIndex(0), _useVariable(false) {;}
  FieldStringElements(const FieldStringElements &inFieldStringElements) :
    _constElement(inFieldStringElements._constElement),
    _variableIndex(inFieldStringElements._variableIndex),
    _variableValue(inFieldStringElements._variableValue),
    _useVariable(inFieldStringElements._useVariable)
    {;}
  
  std::string _constElement;
  int _variableIndex;
  std::string _variableValue;
  bool _useVariable;
};


class Fields
{
private:
  std::vector< FieldStringElements > _fieldElements;
  
  std::string _value;
  std::string _printableValue;
  
  unsigned int _Xposition;
  unsigned int _Yposition;
  unsigned int _Xlength;
  
  int _scrollPosition;
  
  bool _changed;
  bool _scrolled;
  bool _scroll;
    
public:
  Fields();
  Fields( int inXposition, int inXlength, int inYposition );
  Fields( const DeviceMK1::Fields &inField );
  ~Fields() {;}
  
  void makeElements( const std::string &rawString );
  
  void setValue( const std::string &inString, int inVariableNumber );

  unsigned int getXposition() { return _Xposition; } 
  unsigned int getYposition() { return _Yposition; } 
  unsigned int getXlength()   { return   _Xlength; } 
  
  bool getPrintableData( std::string &outPrintableData, bool inForce );
  int scroll();
};
}
}
#endif
